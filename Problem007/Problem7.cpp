#include <iostream>

using namespace std;

bool isPrime(long aNum)
{
  bool ans = true;
  for(int i = 2; i * i <= aNum; i++)
    if(aNum % i == 0)
      ans = false;
  return ans;
}

int main (int argc, char const *argv[])
{
  int primeCount = 1;
  int currTest = 2;
  while(primeCount != 10001){
    currTest++;
    if(isPrime(currTest))
      primeCount++;
  }
  cout << "The 10001st prime: " << currTest << endl;
  return 0;
}