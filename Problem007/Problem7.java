//What is the 10001st prime number?

class Problem7
{
  public static void main(String[] args)
  {
    int primeCount = 1;
    int currTest = 2;
    while(primeCount != 10001){
      currTest++;
      if(isPrime(currTest))
        primeCount++;
    }
    System.out.println("The 10001st prime: " + currTest);
  }
  
  public static boolean isPrime(long aNum)
  {
    boolean ans = true;
    for(int i = 2; i * i <= aNum; i++)
      if(aNum % i == 0)
        ans = false;
    return ans;
  }
}