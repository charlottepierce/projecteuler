//By considering the terms in the Fibonacci sequence whose values do not exceed four million, find the sum of the even-valued terms.
#include <iostream>

using namespace std;

int main()
{
  int a = 1, 
      b = 1, 
      next = 0, 
      sum = 0;
      
  while(next <= 4000000){
    next = a + b;
    if(next % 2 == 0)
      sum += next;
    a = b;
    b = next;
  }
  cout << "Sum: " << sum << endl;
  return 0;
}