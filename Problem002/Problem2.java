//By considering the terms in the Fibonacci sequence whose values do not exceed four million, find the sum of the even-valued terms.
import java.util.ArrayList;

class Problem2
{
  private ArrayList<Integer> fibonacci;
  
  private void generateNumbers()
  {
    fibonacci = new ArrayList<Integer>();
    fibonacci.add(1);
    fibonacci.add(2);
    int x = 0;
    while(x <= 4000000){
      x = fibonacci.get(fibonacci.size() - 2) + fibonacci.get(fibonacci.size() - 1);
      fibonacci.add(x);
    }
  }
  
  private int getSum()
  {
    int result = 0;
    for(Integer i: fibonacci){
      if(i % 2 == 0){
        result += i;
      }
    }
    return result;
  }
  
  public static void main(String[] args)
  {
    Problem2 problem = new Problem2();
    problem.generateNumbers();
    int sum = problem.getSum();
    System.out.println("Sum: " + sum);
  }
}