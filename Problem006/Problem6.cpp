//Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.

#include <iostream>
#include <cmath>

using namespace std;

int main (int argc, char const *argv[])
{
  int squaredTotal = 0;
  int sumTotal = 0;
  for(int i = 1; i <= 100; i++){
    squaredTotal += (i * i);
    sumTotal += i;
  }
  sumTotal *= sumTotal;
  
  cout << "The difference is: " << abs(squaredTotal - sumTotal) << endl;;
  return 0;
}