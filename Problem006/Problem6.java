//Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.

class Problem6
{
  public static void main(String[] args)
  {
    int squaredTotal = 0;
    int sumTotal = 0;
    for(int i = 1; i <= 100; i++){
      squaredTotal += (i * i);
      sumTotal += i;
    }
    sumTotal *= sumTotal;
    System.out.println("The difference is: " + Math.abs((squaredTotal - sumTotal)));
  }
}