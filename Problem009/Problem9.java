// A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
// There exists exactly one Pythagorean triplet for which a + b + c = 1000.
// Find the product abc.

class Problem9
{
	public static void main(String[] args)
	{
		new Problem9().run();
	}
	
	public void run()
	{
		double a, b, c;
		for(a = 1; a != 1000; a++){
			for(b = 1; b != 1000; b++){
				c = Math.hypot(a, b);
				if((a + b + c) == 1000){
					System.out.println("A: " + a);
					System.out.println("B: " + b);
					System.out.println("C: " + c);
					System.out.println("Product: " + (int)(a * b * c));
					System.exit(0);
				}
			}
		}
	}
}