// A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
// There exists exactly one Pythagorean triplet for which a + b + c = 1000.
// Find the product abc.

#include <iostream>
#include <cmath>
#include <cstdio>

using namespace std;
	
int main()
{
	double a, b, c;
	for(a = 1; a != 1000; a++){
		for(b = 1; b != 1000; b++){
			if((a + b) < 1000){
				c = sqrt((a * a) + (b * b));
				if((a + b + c) == 1000){
					cout << "A: " << a << endl;
					cout << "B: " << b << endl;
					cout << "C: " << c << endl;
					cout << "Product: ";
					printf("%.0f", (a * b * c));
					cout << endl;
					return 0;
				}
			}
		}
	}
}