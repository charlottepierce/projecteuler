//2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
//What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?

public class Problem5
{
  public static void main(String[] args)
  {
    int curr = 20;
    while(!check(curr))
      curr += 20;
    System.out.println("The number is: " + curr);
  }

  public static boolean check(int aNum)
  {
    boolean ans = true;
    for(int i = 1; i <= 20; i++){
      if(aNum % i != 0)
        ans = false;
    }
    return ans;
  }
}