// The prime factors of 13195 are 5, 7, 13 and 29.
// What is the largest prime factor of the number 600851475143 ?

#include <iostream>
#include <cmath>

using namespace std;

bool isPrime(long aNum)
{
  for(long i = (aNum / 2) + 1; i > 1; i--)
    if(aNum % i ==  0)
      return false;
  return true;
}

long largestPrimeFactor(long aNum)
{
  for(long i = sqrt(aNum); i > 0; i--)
    if(aNum % i == 0)
      if(isPrime(i))
        return i;
}

int main()
{
  cout << "Largest prime factor: " << largestPrimeFactor(600851475143L) << endl;
  return 0;
}