// The prime factors of 13195 are 5, 7, 13 and 29.
// What is the largest prime factor of the number 600851475143 ?

class Problem3
{
  public static void main(String[] args)
  {
    new Problem3().run();
  }
  
  public void run()
  {
    long num = 600851475143L;
    for(long i = (long)Math.sqrt(num); i > 0; i--){
      if(num % i == 0){
        if(isPrime(i)){
          System.out.println("Largest prime factor: " + i);
          System.exit(0);
        }
      }
    }
  }
  
  private boolean isPrime(long aNum)
  {
    for(long i = (aNum / 2) + 1; i > 1; i--){
      if(aNum % i ==  0){
        return false;
      }
    }
    return true;
  }
}