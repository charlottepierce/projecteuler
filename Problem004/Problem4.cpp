// A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
// Find the largest palindrome made from the product of two 3-digit numbers.
#include <iostream>
#include <string>
#include <sstream>

using namespace std;

string intToString(int i)
{
    stringstream ss;
    string s;
    ss << i;
    s = ss.str();

    return s;
}

bool isPalindrome(int aNum)
{
  string testNum = intToString(aNum);
  for(int i = 0; i < testNum.length(); i++)
    if(testNum[i] != testNum[testNum.length() - (i + 1)])
      return false;
  return true;
}

int main()
{
  int largest = 0;
  for(int i = 999; i >= 100; i--)
    for(int j = 999; j >= 100; j--){
      int next = i * j;
      if(isPalindrome(next))
        if(next > largest)
          largest = next;
    }
  cout << "Largest Palindrome: " << largest << endl;
}