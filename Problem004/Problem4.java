// A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
// Find the largest palindrome made from the product of two 3-digit numbers.

class Problem4
{
  int largest;
  
  public static void main(String[] args)
  {
    new Problem4().run();
  }
  
  public void run()
  {
    largest = 0;
    for(int i = 999; i >= 100; i--){
      for(int j = 999; j >= 100; j--){
        int next = i * j;
        if(isPalindrome(next)){
          if(next > largest){
            largest = next;
          }
        }
      }
    }
    System.out.println("Largest Palindrome: " + largest);
  }
  
  private boolean isPalindrome(int aNum)
  {
    String testNum = String.valueOf(aNum);
    for(int i = 0; i < testNum.length(); i++){
      if(testNum.charAt(i) != testNum.charAt((testNum.length() - (i + 1)))){
        return false;
      }
    }
    return true;
  }
}