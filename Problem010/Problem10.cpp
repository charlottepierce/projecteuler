#include <iostream>

using namespace std;

int main()
{
	int lMax = 2000000;
	long long lSum = 0LL;

	// Array to hold possible prime numbers
	// Made 1 larger than lMax so index matches integer held.
	int *lSieve = new int[lMax + 1];

	//Set initial array values
	for(int i = 1; i < lMax + 1; i++)
		lSieve[i] = i;
	
	//Sieve of Eratosthenes
	for(int i = 2; i < lMax + 1; i++){
		if(lSieve[i] != 0){
			for(int j = i + i; j < lMax + 1; j += i)
				lSieve[j] = 0;
		}
	}

	lSieve[1] = 0; // problem doesn't count 1 as being a prime number
	// Calculate sum
	for(int i = 1; i < lMax + 1; i++)
		lSum += lSieve[i];
	
	cout << lSum << endl;
	return 0;
}