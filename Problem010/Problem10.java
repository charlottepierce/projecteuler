// The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
// Find the sum of all the primes below two million.

public class Problem10
{
	public static void main(String[] args)
	{
		new Problem10().run();
	}

	public void run()
	{
		long lSum = 0;
		for(long i = 3; i < 2000000; i += 2){ // Project euler doesn't count 1 as a prime number.
			if(isPrime(i))
				lSum += i;
		}
		System.out.println("Sum of all primes below two million: " + lSum);
	}

	public static boolean isPrime(long aNum)
	{
		boolean ans = true;
		for(int i = 2; i * i <= aNum; i++)
		if(aNum % i == 0)
			ans = false;
		return ans;
	}
}